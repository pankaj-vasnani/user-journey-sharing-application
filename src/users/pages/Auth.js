import React, {useState, useContext} from 'react';

import './Auth.css';
import Card from '../../shared/components/UIElements/Card';
import Input from '../../shared/components/FormElements/Input';
import Button from '../../shared/components/FormElements/Button';
import { VALIDATOR_EMAIL, VALIDATOR_MINLENGTH, VALIDATOR_REQUIRE} from '../../shared/util/validators';
import useForm from '../../shared/hooks/form-hook';
import {AuthContext} from '../../shared/context/auth-context';
import ErrorModal from '../../shared/components/UIElements/ErrorModal';
import LoadingSpinner from '../../shared/components/UIElements/LoadingSpinner';
import {useHttpClient} from '../../shared/hooks/http-hook';

const Auth = () => {
   const auth = useContext(AuthContext);

   const [isLogin, setIsLogin] = useState(true);

   const {isLoading, error, sendRequest, clearError} = useHttpClient();

   const [formState, inputHandler, setFormData] =  useForm(
        {
            email: {
                value: '',
                isValid: false
            }, 
            password: {
                value: '',
                isValid: false
            }
        },
        false
    )

    const authSubmitHandler = async event => {
        event.preventDefault();

        if(isLogin) {
            try {
                await sendRequest(
                    'http://localhost:5000/api/users/login', 
                    'POST',
                    JSON.stringify({
                        email: formState.inputs.email.value,
                        password: formState.inputs.password.value
                    }),
                    {
                        'Content-Type': 'application/json'
                    },
                );

                auth.login();
            } catch(err) {

            }
        } else {
            try {
                await sendRequest(
                    'http://localhost:5000/api/users/', 
                    'POST',
                    JSON.stringify({
                        image: "https://cdn.pixabay.com/photo/2015/04/23/22/00/tree-736885__340.jpg",
                        name: formState.inputs.name.value,
                        places: ["374834sjdsjdhsjhdsjd"],
                        email: formState.inputs.email.value,
                        password: formState.inputs.password.value
                    }),
                    {
                        'Content-Type': 'application/json'
                    }
                );
                auth.login();
                
            } catch(err) {
            
            }
        }
    }

    const switchModeHandler = event => {
        event.preventDefault();

        if(!isLogin) {
            setFormData(
                {
                   ...formState.inputs,
                   name: undefined
                }, formState.inputs.email.isValid && formState.inputs.password.isValid
            )
        } else {
            setFormData(
                {
                   ...formState.inputs,
                   name:{
                       value: '',
                       isValid: false
                   }
                }, false
            )
        }

        setIsLogin(prevMode => !prevMode)
    }

    return (
        <React.Fragment>
            <ErrorModal error={error} onClear={clearError}/>
            <Card className="authentication">  
                {isLoading && <LoadingSpinner asOverLay/>}
                <h2> Login Required</h2>
                <form onSubmit={authSubmitHandler}>
                    {
                        !isLogin &&
                        <Input id="name" element="input" type="text" label="Your Name" validators={[VALIDATOR_REQUIRE]} errorText="Please enter valid name" onInput={inputHandler} />
                    }
                    <Input element="input" id="email" type="email" label="Enter E-Mail" validators={[VALIDATOR_EMAIL()]} errorText="Please enter valid email address" onInput={inputHandler}/>

                    <Input element="input" id="password" type="password" label="Enter Password" validators={[VALIDATOR_MINLENGTH(5)]} errorText="Please enter valid password" onInput={inputHandler}/>

                    {/* <Button type="submit" disabled={!formState.isValid}>
                        {!isLogin ? 'Login' : 'SignUp' }
                    </Button> */}
                    <Button type="submit">
                        {isLogin ? 'Login' : 'SignUp' }
                    </Button>
                    <Button inverse onClick={switchModeHandler}>
                        Switch to {!isLogin ? 'Login' : 'SignUp' } Mode
                    </Button>
                </form>
            </Card>
        </React.Fragment>
    );
}

export default Auth;