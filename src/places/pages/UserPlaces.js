import React from 'react';
import PlaceList from '../components/PlaceList';
import {useParams} from 'react-router-dom';

const UserPlaces = (props) => {
    const userPlaces = [
        {
            id: '1',
            name: 'Eiffel Tower',
            imageUrl: 'https://media.tacdn.com/media/attractions-splice-spp-674x446/06/74/aa/fc.jpg',
            description: "place description",
            address: "Champ de Mars, 5 Avenue Anatole France, 75007 Paris, France",
            creator: "u1",
            location: {
                lat: 48.8583701,
                lng: 2.2922926
            }
        },
        {
            id: '2',
            name: 'Essel world Amusement Park',
            imageUrl: 'https://hellotravel.gumlet.com/images/things2do/738X538/essel-world-entry-ticket-price(youthgiri)_1517162609t.jpg?w=480&dpr=2.6',
            description: "place description",
            address: "Gorai Mumbai, Maharashtra, India",
            creator: "u2",
            location: {
                 lat: 19.2286406 ,
                 lng: 72.7863848 
            } 
        }
    ]

    const userId = useParams().userId
    const loadedPlaces = userPlaces.filter(place => place.creator === userId)

    return(
        <PlaceList items={loadedPlaces } />
    );
};

export default UserPlaces;
