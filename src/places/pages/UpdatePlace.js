import React, {useEffect, useState} from "react";
import {useParams} from 'react-router-dom';
import Input from '../../shared/components/FormElements/Input';
import Button from '../../shared/components/FormElements/Button';
import {
  VALIDATOR_REQUIRE,
  VALIDATOR_MINLENGTH
} from '../../shared/util/validators';
import "./PlaceForm.css";
import useForm from '../../shared/hooks/form-hook';
import Card from '../../shared/components/UIElements/Card';

const UpdatePlace = props => {
    const [isLoading, setIsLoading] = useState(true);

    const userPlaces = [
        {
            id: '1',
            name: 'Eiffel Tower',
            imageUrl: 'https://media.tacdn.com/media/attractions-splice-spp-674x446/06/74/aa/fc.jpg',
            description: "place description",
            address: "Champ de Mars, 5 Avenue Anatole France, 75007 Paris, France",
            creator: "u1",
            location: {
                lat: 48.8583701,
                lng: 2.2922926
            }
        },
        {
            id: '2',
            name: 'Essel world Amusement Park',
            imageUrl: 'https://hellotravel.gumlet.com/images/things2do/738X538/essel-world-entry-ticket-price(youthgiri)_1517162609t.jpg?w=480&dpr=2.6',
            description: "place description",
            address: "Gorai Mumbai, Maharashtra, India",
            creator: "u2",
            location: {
                 lat: 19.2286406 ,
                 lng: 72.7863848 
            } 
        }
    ]

    const placeId = useParams().placeId;

    const [formState, inputHandler, setFormData] = useForm
    (
        {
            title: {
                value: '',
                isValid: false
            },
            description: {
                value: '',
                isValid: false
            }
        },
        false
    )

    const identifiedPlace = userPlaces.find(p => p.id === placeId)
    
    useEffect(() => {
        if(identifiedPlace) {
            setFormData(
                {
                    title: {
                        value: identifiedPlace.name,
                        isValid: true
                    },
                    description: {
                        value: identifiedPlace.description,
                        isValid: true
                    }
                },
                true
            );  
        }
        setIsLoading(false);
    }, [setFormData, identifiedPlace])

    if(! identifiedPlace) {
        return (
            <div className="text-center">
                <Card>
                    <h2>
                        Could not find the place
                    </h2>
                </Card>
            </div>
        )
    }

    const formSubmitHandler = (event) => {
        event.preventDefault();

        console.log(formState.inputs);
    }

    if(isLoading) {
        return (
            <div className="center">
                <h2>Loading...</h2>
            </div>
        )
    }

    return (
        <form className="place-form" onSubmit={formSubmitHandler}>
            <Input
                id="title"
                element="input"
                type="text"
                label="Title"
                validators={[VALIDATOR_REQUIRE()]}
                errorText="Please enter a valid title."
                onInput={inputHandler}
                initialValue={formState.inputs.title.value}
                initialValid={formState.inputs.title.isValid}
            />
            <Input
                id="description"
                element="textarea"
                label="Description"
                validators={[VALIDATOR_MINLENGTH(5)]}
                errorText="Please enter a valid description (at least 5 characters)."
                onInput={inputHandler}
                initialValue={formState.inputs.description.value}
                initialValid={formState.inputs.description.isValid}
            />

            <Button type="submit" disabled={! formState.isValid}>
                UPDATE PLACE
            </Button>
        </form>
    )
}

export default UpdatePlace;