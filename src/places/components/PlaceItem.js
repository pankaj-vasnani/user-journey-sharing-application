import React, {useState, useContext} from 'react';
import Card from '../../shared/components/UIElements/Card';
import Button from '../../shared/components/FormElements/Button';
import Modal from '../../shared/components/UIElements/Modal';
import Map from '../../shared/components/UIElements/Map';
import {AuthContext} from '../../shared/context/auth-context';

const PlaceItem = (props) => {
    const auth = useContext(AuthContext);

    const [showMap, setShowMap] = useState(false);

    const openMapHandler = () => setShowMap(true);

    const closeMapHandler = () => setShowMap(false);

    const [showDeleteModal, setShowDeleteModal] = useState(false);

    const openDeleteModalHandler = () => setShowDeleteModal(true);

    const closeDeleteModalHandler = () => setShowDeleteModal(false);

    const confirmDeleteHandler = () => {
        console.log('Place Successfully Deleted');

        setShowDeleteModal(false);
    }

    return(
        <React.Fragment>
            <Modal show={showMap} onCancel={closeMapHandler} header={props.address} contentClass="place-item__modal-content"
            footerClass="place-item__modal-actions"
            footer={<Button onClick={closeMapHandler}>CLOSE</Button>} 
            >
                <div className="map-container">
                   <Map lat={props.coordinates['lat']} lng={props.coordinates['lng']} zoom={14} title={props.name}/>
                </div>
            </Modal>
            <Modal header="Are you sure ?"
            show={showDeleteModal} 
            onClick={closeDeleteModalHandler}
            className="place-item__modal-actions" footer={
                <React.Fragment>
                    <Button inverse onClick={closeDeleteModalHandler}>
                        CANCEL
                    </Button>
                    <Button inverse onClick={confirmDeleteHandler}>
                        DELETE
                    </Button>
                </React.Fragment>
            }>
                <p>
                    Do you want to proceed with the deletion of the place ?
                </p>
            </Modal>
            <li className='place-item'>
                <Card className="place-item__content">
                    <div className='place-item__image'>
                        <img src={props.image} alt={props.name} />
                    </div>
                    <div className="place-item__image">
                        <h2>
                            {props.name}
                        </h2>
                        <h3>
                            {props.address}
                        </h3>
                        <p>
                            {props.location}
                        </p>
                    </div>
                    <div className="place-item__actions">
                        <Button inverse onClick={openMapHandler}>
                            VIEW ON MAP
                        </Button>
                        {
                            auth.isLoggedIn &&
                            <React.Fragment>
                                <Button to={`/places/${props.id}`}>
                                    EDIT
                                </Button>
                                <Button danger onClick={openDeleteModalHandler}>
                                    DELETE
                                </Button>
                            </React.Fragment>
                        }
                    </div>
                </Card>
            </li>
        </React.Fragment>
    )
}

export default PlaceItem;