import React, { useState } from 'react';
import {Link} from 'react-router-dom';
import './MainNavigation.css';
import MainHeader from './MainHeader';
import NavLinks from './NavLinks';
import SideBar from './SideBar';
import BackDrop from '../UIElements/Backdrop';

const MainNavigation = (props) => {
    const [ drawerIsOpen, setDrawerIsOpen ] = useState(false);

    const openDrawerHandler = () => {
        setDrawerIsOpen(true);
    }

    const closeDrawerHandler = () => {
        setDrawerIsOpen(false);
    }

    return(
        <React.Fragment>
            { drawerIsOpen && <BackDrop onClick={closeDrawerHandler} /> }
            {
                drawerIsOpen 
                    ? 
                (
                    <SideBar show={drawerIsOpen} onClick={closeDrawerHandler}>
                        <nav className="main-navigation__drawer-nav">
                            <NavLinks />
                        </nav>
                    </SideBar>
                ) 
                : null 
            }
            <MainHeader>
                <button  className='main-navigation__menu-btn' onClick={openDrawerHandler}>
                    <span />
                    <span />
                    <span />
                    <h1 className='main-navigation__title'>
                        <Link to="/">Your Places</Link>
                    </h1>
                    <nav className="main-navigation__header-nav">
                        <NavLinks />
                    </nav>
                </button>
            </MainHeader>
        </React.Fragment>
    );
}


export default MainNavigation;