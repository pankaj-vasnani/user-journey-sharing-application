import React from 'react';
import './Map.css';

const Map = props => {
    return (
        <div className={`map ${props.className}`} style={props.style}>
            <iframe
                title = {props.title} 
                width="300" 
                height="170" 
                frameborder="0" 
                scrolling="no" 
                marginheight="0" 
                marginwidth="0" 
                src={`https://maps.google.com/maps?q=${props.lat},${props.lng}&hl=es&z=${props.zoom}&amp;output=embed`}
                >
                </iframe>
        </div>
    )
}

export default Map;